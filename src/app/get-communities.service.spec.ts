import { TestBed } from '@angular/core/testing';

import { GetCommunitiesService } from './get-communities.service';

describe('GetCommunitiesService', () => {
  let service: GetCommunitiesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetCommunitiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
