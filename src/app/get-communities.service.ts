import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GetCommunitiesService {

getCommunitiesUrl:string="http://localhost:4200/api/get-community";
constructor(private http:HttpClient) { }
getCommunities()
  {
    let formData=new FormData()
    formData.append('server_key','9f1c90293f914071950e63cc6be50e75'),
    formData.append('access_token','3e6bf755a33a0aab18671b6d89720c0f3bb854014711db2d1c60d9da206cde29633e2a6c29557447e1226495c14f1a62ae17aa76c1f0d457'),
    formData.append('fetch','groups'),
    formData.append('limit','10'),
    formData.append('pageNo','1');
    return this.http.post(this.getCommunitiesUrl,formData)
  }
} 
