import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GetCommunitiesService } from '../get-communities.service';
@Component({
  selector: 'app-my-communities',
  templateUrl: './my-communities.component.html',
  styleUrls: ['./my-communities.component.css']
})
export class MyCommunitiesComponent implements OnInit {

  communityData: Object;

  constructor(private http:HttpClient,private getCommunitiesService:GetCommunitiesService) { }

  ngOnInit(): void {
   
    this.getCommunitiesService.getCommunities().subscribe(data=>{
     console.log(data);
      this.communityData=data;
    })

  }

}
