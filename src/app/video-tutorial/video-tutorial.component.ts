import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-video-tutorial',
  templateUrl: './video-tutorial.component.html',
  styleUrls: ['./video-tutorial.component.css']
})
export class VideoTutorialComponent implements OnInit {
  @ViewChild('iframe') iframe: ElementRef;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
  }
  onClose() {
    const src = this.iframe.nativeElement.getAttribute('src');
    this.renderer.setAttribute(this.iframe.nativeElement, 'src', src);
  }

}
