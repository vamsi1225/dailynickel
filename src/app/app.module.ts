import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchForCommunityComponent } from './search-for-community/search-for-community.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { ProductFilterPipe } from './product-filter.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MyCommunitiesComponent } from './my-communities/my-communities.component';
import { SearchForProductsAndLocationComponent } from './search-for-products-and-location/search-for-products-and-location.component';
import { VideoTutorialComponent } from './video-tutorial/video-tutorial.component';





@NgModule({
  declarations: [
    AppComponent,
    SearchForCommunityComponent,
    SearchbarComponent,
    ProductFilterPipe,
    MyCommunitiesComponent,
    SearchForProductsAndLocationComponent,
    VideoTutorialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    GooglePlaceModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey:"AIzaSyCeBqtMAUxyssutXoIsy6x4SRsZgKEMu8w",
      libraries:['places']
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
