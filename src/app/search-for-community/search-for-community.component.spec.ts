import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchForCommunityComponent } from './search-for-community.component';

describe('SearchForCommunityComponent', () => {
  let component: SearchForCommunityComponent;
  let fixture: ComponentFixture<SearchForCommunityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchForCommunityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchForCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
