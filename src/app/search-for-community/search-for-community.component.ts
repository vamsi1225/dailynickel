import { Component, OnInit } from '@angular/core';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { HttpClient } from '@angular/common/http';
import { GetCommunitiesService } from '../get-communities.service';

@Component({
  selector: 'app-search-for-community',
  templateUrl: './search-for-community.component.html',
  styleUrls: ['./search-for-community.component.css']
})
export class SearchForCommunityComponent implements OnInit {

  constructor(private http:HttpClient,private getCommunityService:GetCommunitiesService) { }
  communityData:any;
  term:any;
  selectedCommunityId:any;
  selectedCommunity:any='';
  searchvalue:any="";

  ngOnInit(): void {
    this.getCommunityService.getCommunities().subscribe(data=>{
      this.communityData=data;
    })
  }
  selectCommunity(value)
  {
    console.log("clicked",value)
  this.selectedCommunity=value;
  }
  showDropDown(communityList)
  {
      if(communityList)
      {
        return "menu2"
      }
      else
      {
        return "hide"
      }
  }

  search()

  {
    console.log(this.selectedCommunity)
    this.searchvalue=this.selectedCommunity
  }

}
