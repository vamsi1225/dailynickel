import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchForProductsAndLocationComponent } from './search-for-products-and-location.component';

describe('SearchForProductsAndLocationComponent', () => {
  let component: SearchForProductsAndLocationComponent;
  let fixture: ComponentFixture<SearchForProductsAndLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchForProductsAndLocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchForProductsAndLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
